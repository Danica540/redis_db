const express = require('express');
const expressHandlebars = require('express-handlebars');
const path = require('path');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const redis = require('redis');
const Handlebars = require('handlebars');

let currentRoom = "";
const letters = ['A', 'B', 'C', 'D', 'E',
    'F', 'G', 'H', 'I', 'J',
    'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y',
    'Z'];

// Redis client
let client = redis.createClient(); // za pristup bazi 16379,'localhost'
let pub = redis.createClient();
let sub = redis.createClient();

// Redis PUB/SUB
client.on('connect', () => {
    console.log("REDIS: Redis is connected.");
});

client.on('error', function (err) {
    console.log('REDIS: Something went wrong ' + err);
});

// Subscribe
sub.on("subscribe", (channel, count) => {
    console.log("REDIS: Subscribed to " + channel + ". Now subscribed to " + count + " channel(s).");
});

// Init app
const app = express();

// Static files
app.use(express.static('public'));

// View engine
app.engine('handlebars', expressHandlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

// Custom helper
Handlebars.registerHelper('ifValue', function (val1, val2, options) {
    if (val1 == val2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});
Handlebars.registerHelper('ifNotValue', function (val1, val2, options) {
    if (val1 != val2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Method override
app.use(methodOverride('_method'));

// Set port
const port = process.argv.slice(2)[0];

// Socket.io 
const server = require('http').createServer(app);
const io = require('socket.io')(server);
io.on('connection', (socket) => {
    //console.log('SOCKET: A user connected');
    socket.on('disconnect', () => {
        //console.log('SOCKET: User disconnected');
    });
    socket.on('guessing word c2s', (word) => {
        console.log('SOCKET:  primljen guessing word c2s');
        client.set("wordToGuess", word);
        pub.publish("wordToGuess", word);
    });
    socket.on('letter c2s', (word, letter) => {
        console.log('SOCKET:  primljen letter c2s');
        client.lpush("letters" + currentRoom, letter);
        pub.publish('guessedLetters', letter);
    });
    socket.on('game score c2s', (result) => {
        console.log('SOCKET:  primljen game score c2s');
        console.log(result);
        client.del(result, function (err, response) {
            if (response == 1) {
                console.log("Deleted Successfully!")
            } else {
                console.log("Cannot delete")
            }
        })
    })

});
// REDIS subscriber
sub.on("message", (channel, message) => {
    console.log('REDIS:  ');
    if (channel == 'wordToGuess') {
        console.log('SOCKET:  salje guessing word s2c');
        io.emit("guessing word s2c", message);
    }
    else if (channel == 'guessedLetters') {
        console.log('SOCKET:  salje guessed letters s2c');
        client.lrange("letters" + currentRoom, 0, -1, (err, list) => {
            io.emit('guessed letters s2c', list);
        });
    }
});
server.listen(port, () => { console.log("Server started on port " + port) });


// Guest number
let userName = "guest";
client.get('guestId', (err, result) => {
    if (err) {
        res.render('error', { error: "Doslo je do greske. Ne mozete pristupiti sajtu." })
    }
    else {
        if (!result) {
            client.set("guestId", '0');
            userName += '0';
        }
        else {
            userName += result;
        }
        client.incr("guestId");
    }
})


// Home page
app.get('/', (req, res, next) => {
    getHomePage(res);
});

// Create a room
app.post('/create', (req, res, next) => {
    makeARoom(res);
})

// Join a room
app.post('/join', (req, res, next) => {
    joinARoom(res, req);
})

//Functions
function joinARoom(res, req) {
    const roomName = req.body.name;
    currentRoom = roomName;
    let twoPlayers = false, numberOfPlayers = 0;
    let promises = [];
    let roomObj = {};

    promises.push(new Promise(function (resolve, reject) {
        client.hgetall(roomName, (err, obj) => {
            if (err) {
                res.render('error', { error: "Greska! Ne mozete pristupiti sobi." });
            }
            else {
                if (!obj) {
                    res.render('error', { error: "Greska! Soba ne postoji." });
                }
                else {
                    roomObj = obj;
                    let listOfPlayers = JSON.parse(roomObj.listOfPlayers);
                    numberOfPlayers = parseInt(roomObj.numberOfPlayers);
                    if (!listOfPlayers.includes(userName)) {
                        listOfPlayers.push(userName);
                        roomObj.listOfPlayers = listOfPlayers;
                        client.hset(roomName, 'listOfPlayers', JSON.stringify(roomObj.listOfPlayers));
                    }
                    numberOfPlayers = parseInt(roomObj.numberOfPlayers) + 1;
                    roomObj.numberOfPlayers = numberOfPlayers;
                    client.hset(roomName, 'numberOfPlayers', numberOfPlayers);
                    twoPlayers = (numberOfPlayers == 2);
                    if (!twoPlayers) {
                        roomObj.master = userName;
                        client.hset(roomName, 'master', userName);
                        sub.subscribe('wordLetters');
                        sub.subscribe('guessedLetters');
                    }
                    else {
                        sub.subscribe('wordToGuess');
                        sub.subscribe('guessedLetters');
                    }
                    resolve(obj);
                }
            }
        })
    }));


    Promise.all(promises).then(function (values) {
        if (values[0].numberOfPlayers == 2) {
            twoPlayers = true;
        }
        else {
            let timer = setInterval(() => {
                promise = new Promise(function (resolve, reject) {
                    client.hgetall(roomName, (err, obj) => {
                        resolve(obj);
                    })
                });
                promise.then(function (val) {
                    if (val.numberOfPlayers == '2') {
                        twoPlayers = true;
                        clearInterval(timer);
                        res.render('room', { room: val, currentUser: userName });
                    }
                })
            }, 2000);
        }
        if (twoPlayers) {
            res.render('room', { room: roomObj, currentUser: userName, letters: letters });
        }
    });
}

function getHomePage(res) {
    client.scan(0, "MATCH", "room*", 'COUNT', 100, (error, result) => {
        if (error) {
            res.render('error', { error: "Doslo je do greske prilikom pribavljanja soba." })
        }
        else {
            let allHashes = [];
            result[1].forEach(key => {
                client.type(key, (error, type) => {
                    if (!error) {
                        if (type == "hash") {
                            client.hgetall(key, (err, hash) => {
                                if (!err) {
                                    hash.name = key;
                                    if (hash.numberOfPlayers < 2) {
                                        allHashes.push(hash);
                                    }
                                }
                            })
                        }
                    }
                })
            })
            res.render('home', { hashes: allHashes });
        }
    })
}

function makeARoom(res) {
    client.get('roomId', (error, result) => {
        if (error) {
            res.render('error', { error: "Doslo je do greske prilikom pribavljanja id-ja sobe." });
        }
        if (!result) {
            client.set("roomId", "0");
            result = '0';
        }
        const roomName = "room" + result;
        client.incr("roomId");
        client.hset(roomName, 'numberOfPlayers', 0);
        client.hgetall(roomName, (err, obj) => {
            console.log(obj);
            if (!obj) {
                res.render('error', { error: "Greska! Soba ne postoji." });
            }
            else {
                let roomObj = obj;
                roomObj.name = roomName;
                client.hset(roomName, 'name', roomName);
                let listOfPlayers = [];
                listOfPlayers.push(userName);
                roomObj.listOfPlayers = listOfPlayers;
                client.hset(roomName, 'listOfPlayers', JSON.stringify(roomObj.listOfPlayers));
                getHomePage(res);
            }
        })
    });
}



