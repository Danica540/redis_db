let socket = io();

$(function () {
    $('#submitWordBtn').click(function (e) {
        e.preventDefault(); // prevents page reloading
        let word = $('#wordInput').val();
        console.log('SOCKET:  salje guessing word c2s');
        socket.emit('guessing word c2s', word.toUpperCase());
        $('#wordInput').val('');
        writeWordOnCanvas(word, null);
        document.getElementById('submitWordBtn').disabled = true;
    });
});

if (document.getElementById("submitWordBtnClient")) {
    document.getElementById("submitWordBtnClient").onclick = (e) => {
        e.preventDefault(); // prevents page reloading
        let word = document.getElementById("canvas").value;
        let clientWord = document.getElementById("wordInputClient").value;
        if (clientWord.toUpperCase() == word.toUpperCase()) {
            for(let i=0;i<word.length;i++){
                socket.emit('letter c2s', word, word.charAt(i));
            }
            endGame("Player has won!");
        }
        else {
            document.getElementById('wrongAnswer').style.display = 'block';
            setTimeout(() => {
                document.getElementById('wrongAnswer').style.display = 'none';
            }, 5000);
        }
    }
}

let buttons = document.querySelectorAll('.letter');
if (buttons) {
    buttons.forEach(btn => {
        btn.onclick = (e) => {
            if (btn.value == e.target.value) {
                let word = document.getElementById("canvas").value;
                console.log('SOCKET:  salje letter c2s');
                socket.emit('letter c2s', word, btn.value);
                btn.disabled = true;
            }
        }
    })
}


socket.on('guessing word s2c', (word) => {
    writeWordOnCanvas(word.toUpperCase(), null);
    enableButtons();
});

socket.on('guessed letters s2c', (guessedLetters) => {
    let word = document.getElementById("canvas").value;
    writeWordOnCanvas(word.toUpperCase(), guessedLetters);
});

function writeWordOnCanvas(word, guessedLetters) {
    let canvas = document.getElementById("canvas");
    let numberOfGuessedWords = 0;
    let numberOfMissedGuesses = 0;
    canvas.value = word;
    let ctx = canvas.getContext("2d");
    ctx.font = "22px Arial";
    ctx.textAlign = "center";
    let ofsetX = 22, ofsetY = canvas.height / 2;
    if (!guessedLetters) {
        for (let i = 0; i < word.length; i++) {
            if (word.charAt(i) == ' ') {
                ofsetX += 22;
                ctx.fillText(' ', ofsetX, ofsetY);
            }
            else {
                ctx.fillText('_', ofsetX, ofsetY);
            }
            ofsetX += 22;
        }
    }
    else {
        for (let i = 0; i < word.length; i++) {
            if (guessedLetters.includes(word.charAt(i))) {
                ctx.fillText(word.charAt(i), ofsetX, ofsetY);
                numberOfGuessedWords++;
            }
            else if (word.charAt(i) == ' ') {
                ofsetX += 22;
                ctx.fillText(' ', ofsetX, ofsetY);
            }
            else {
                ctx.fillText('_', ofsetX, ofsetY);
            }
            ofsetX += 22;
        }
    }

    if (guessedLetters) {
        guessedLetters.forEach(l => {
            if (word.indexOf(l) == -1) {
                numberOfMissedGuesses++;
            }
        })
    }

    let numberOfSpaces = 0;
    for (let i = 0; i < word.length; i++) {
        if (word.charAt(i) == ' ') {
            numberOfSpaces++;
        }
    }

    if (numberOfGuessedWords == word.length - numberOfSpaces) {
        endGame("Player has won!");
    }
    if (guessedLetters) {
        // 7 pokusaja
        if (numberOfMissedGuesses == 6) {
            endGame("Player has lost!");
        }
    }
}

function endGame(string) {
    if (string == "Player has lost!") {
        disableButtons();
    }
    setTimeout(() => {
        alert(string);
    }, 3000)
    socket.emit('game score c2s', document.getElementById('roomName').innerHTML.replace(/\s/g,''));

}

function disableButtons() {
    document.getElementById("submitWordBtnClient").disabled = true;
    let buttons = document.querySelectorAll(".letter");
    if (buttons) {
        buttons.forEach(btn => {
            btn.disabled = true;
        })
    }
}

function enableButtons() {
    document.getElementById("submitWordBtnClient").disabled = false;
    let buttons = document.querySelectorAll(".letter");
    if (buttons) {
        buttons.forEach(btn => {
            btn.disabled = false;
        })
    }
}